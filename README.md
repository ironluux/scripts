# Custom Scripts

This is a place for my custom scripts, mostly written in bash. Some are written in python. These are intended to make my life easier and hopefully make others lives easier too!

### Scripts Navigation:
- Custom Scripts Folder Contents:
    - ping-cron.sh - Used for logging ping failures, intended for cron
    - check-ip-location.py (written in Python) - Used to plug in several IPs from a .xls to determine the City/State of US based IPs.
    - check-ssl-expiration-date.sh - Used to check the expiration date of a website, very good if you have over 10+ domains to check yearly
- Backup Script Folder:
    - These scripts are intended to use on Linux systems, it creates a .sfs of the file system while it's running. SquashFS is versatile and can be mounted, so files can be pulled off the backup.
- Python Practice Folder:
    - A few scripts I made while learning python
- Sea Searcher Folder:
    - A script that runs searches on a search engine randomly, uses selenium.
- Ansible Labs Folder:
    - Files and scripts written while labbing out/learning ansible. These are very practical and simple.
