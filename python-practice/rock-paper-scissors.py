#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# ----------------------------------------------------------------------------------------------------------

import random

choices = ["Rock", "Paper", "Scissors"]

user_choice_input = int(input("What's your choice, 0 for rock\n1 for paper\n2 for scissors\n"))
if user_choice_input > 3 or user_choice_input < 0:
    print("That's not a valid choice, you lose.\n")
    exit()
user_choice = choices[user_choice_input]
computer_choice = choices[random.randint(0,2)]
draw = "It's a draw!"
you_lose = "You Lose, good day sir!"
you_win = "Look at that, you won!"
print("You chose " + user_choice)
print("The computer chose " + computer_choice)

if user_choice == computer_choice:
    print(draw)
elif user_choice == choices[0] and computer_choice == choices[1]:
    print(you_lose)
elif user_choice == choices[0] and computer_choice == choices[2]:
    print(you_win)
elif user_choice == choices[1] and computer_choice == choices[0]:
    print(you_win)
elif user_choice == choices[1] and computer_choice == choices[2]:
    print(you_lose)
elif user_choice == choices[2] and computer_choice == choices[0]:
    print(you_lose)
elif user_choice == choices[2] and computer_choice == choices[1]:
    print(you_win)
else:
    print("Oh no, that's not a valid entry. Please pick from 0-2")
