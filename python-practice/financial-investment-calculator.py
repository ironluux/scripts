#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# ----------------------------------------------------------------------------------------------------------

# Created to keep my finances straight.
# This script calculates the amount I need to invest in each month to which stock/index fund

# Asking the user what they want to invest
initial_amount = float(input("How much do you have to invest?\n$"))

# Biggest investment
us_inv = initial_amount * .7
us_inv_rounded = "{:.2f}".format(us_inv)
# International or secondary investment
int_inv = initial_amount * .2
int_inv_rounded = "{:.2f}".format(int_inv)
# Bonds
bond_inv = initial_amount * .1
bond_inv_rounded = "{:.2f}".format(bond_inv)

print(f"\n\nInvestments\nStock 1 (US Index): ${us_inv_rounded}\nStock 2 (International): ${int_inv_rounded}\nStock 3 (US Bonds): ${bond_inv_rounded}\n\nDone! Have a great day!")
