#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# ----------------------------------------------------------------------------------------------------------


# This is my choose your own adventure game.

print('''
      .
     J:L    (""")
     |:|     III
     |:|     III
     |:|     III
     |:|   __III__
     |:| /:-.___,-:|
     |:| \]  |:|  [/
     |:|     |:|
     |:|     |:|
     |:|     |:|
 /]  |:|  [\ |:|
 \:-'"""`-:/ |:|
   ""III""   |:|
     III     |:|
     III     |:|
     III     |:|
    (___)    J:F
              "                       
''')

print ("Welcome to Prison Island, continue if you dare\n")

choice1 = input("You have woken up in a dungeon hallway, the guard is dead in your cell behind you. The door is wide open. Do you go left or right?\n")


if choice1.lower() == "right":
    choice2 = input("You arrive outside, huzzah! But you're on an island. There's a broken down boat. Do you take the boat or swim?\n")
    if choice2.lower() == "swim":
        choice3 = input("You made it to the other side of the island. There are 3 portals. Which portal should you jump through? Red, green, or blue?\n")
        if choice3.lower() == "red":
            print("You woke back up in your cell. The guard is alive and the door is locked.\nGame over.\nThanks for playing!")
        elif choice3 == "blue":
            print("You are teleported thousands of feet into the air! You scream, but it doesn't matter. You keep falling for awhile. Eventually you get bored and accept your fate.\n\nSplat.\nGame over, thanks for playing!")
        elif choice3 =="green":
            print("You wake up inside a green crystal. There's no escape. You eventually will die of starvation. But for now, it's peaceful and you are free.\nGame over\nThanks for playing!")
        else:
             print("You didn't make the right choice, and because of that, you get killed by an orc.\nBad luck\nGame Over!")
    elif choice2.lower() == "boat":
        print("Your boat starts taking on water. Oh no! It's too late to do anything. You sink and your leg gets caught on a rope. No one can hear your underwater screams.\nGame over\nThanks for playing!")
    else:
         print("You didn't make the right choice, and because of that, you get killed by an orc.\nBad luck\nGame Over!")
elif choice1.lower() == "left":
    print("There's an orc right in front of you. You just found out who killed the guard. After trying to fight back unarmed, you die a brutal and terrible death.\nGame over\nThanks for playing!")
else:
     print("You didn't make the right choice, and because of that, you get killed by an orc.\nBad luck\nGame Over!")
