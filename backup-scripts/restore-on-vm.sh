#!/bin/bash
#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# ----------------------------------------------------------------------------------------------------------
# Created on 1/4/2022. This script is used to wipe
# a partition of a VM for easy restoral of a backup onto a VM. It
# may be edited in the future to allow for really quick partition structure
# creation in the event of a backup restore scenario.

# Lists devices and asks the user which one they want to wipe

RED="\e[31m"
ENDCOLOR="\e[0m"
while true; do
lsblk
read -p "What device do you want to edit?
> " DEVICE

echo -e "Editing ${RED}$DEVICE${ENDCOLOR}, this will wipe ${RED}$DEVICE${ENDCOLOR} of all its data!!! ${RED}ARE YOU ABSOLUTELY SURE??${ENDCOLOR}"

while true; do
	read -p "Type yes in all caps to continue
Type no to start over
Type exit to exit the script
!!> " ANSWER
	case $ANSWER in 
		[YES]* ) wipefs -a $DEVICE;
			/sbin/parted "$DEVICE" mklabel msdos --script;
			/sbin/parted "$DEVICE" mkpart primary 0% 100% --script;
			/sbin/parted "$DEVICE" set 1 boot on --script;
			mkfs.ext4 "$DEVICE"1;
			echo "All done!"
			exit;;
		[exit]* ) echo "Exiting script..."; exit;;
		* ) break;;
	esac
done

#breaks eternal loop
done
echo "All done!"

exit 0
