Full Backup/Restore Guide
====================

This guide is the backup and restore process for encrypted ubuntu based servers. A custom made script backs up a running linux machine to a squashfs file for easy recovery.

The Backup Process
====================
A backup script in the .scripts folder rsyncs all files incrementally, compresses the file, moves it to an archive, and deletes old archives.
Initial Setup
====================
Run the initial_setup.sh command, or if you don't trust me, do the actual commands:
+ Make /backups/local_backup and /backups/local_backup_archive folder in root
+ Create a file called LVM_Structure.bak and run these commands as root in terminal:
+ pvs, vgs, lvs, lsblk, gdisk -l /dev/sdx, fdisk -l /dev/sdx, parted -l
+ Copy the output to the LVM_Structure.bak file
+ Set up a root cron job to run this script.
+ Exclude any folders you want in the script and run it once as root
+ If everything goes well, you're done!


Restoration
====================
Restoration can be done in several ways. If you haven’t lost all files, then you should just boot from a live USB and do an rsync on the encrypted drive. You can unlock the drive and rsync straight into root. But, let’s assume something catastrophic has happened with the drive. Or, you may want to do this to add encryption to the drive. Let’s cover all the recovery ways.

Refresh
====================
+ If the partitions are still intact and you just want to revert on a backup, follow these steps. 
+ In a live ubuntu USB, run:
```
mkdir /mnt/usb
mkdir /mnt/system/
mkdir /mnt/squash
cryptsetup luksOpen /dev/sdx6 root
lsblk (to get your lvm structure)
mount /dev/mapper/your-vg--your-lv /mnt/system
mount -o loop -t squashfs /mnt/usb/path/to/backup/backup.sfs /mnt/squash
rsync -aAXHv --exclude="lost+found" /mnt/squash/* /mnt/system/ (this will take awhile)
umount /mnt/usb
umount /mnt/system
umount /mnt/squash
Reboot, you’re done!
```
VM Restore
====================
You may just want to restore into a VM and spin that up as a temporary solution. There's a script that's included titled  "restore-on-vm.sh"
```
+ Launch a ubuntu live iso
+ Plug in your thumbdrive with the script and backup
+ Pass the USB through to the VM
+ run bash restore-on-vm.sh and follow the prompts
+ follow the Refresh steps above
```

Chroot:
---------------------
```
rm /mnt/system/etc/resolve.conf 
cp /etc/resolve.conf /mnt/system/etc/
for i in {dev,dev/pts,proc,sys}; do mount --bind /$i /mnt/system/$i; done
chroot /mnt/system
```

Crypttab:
---------------------
```
Edit the /etc/crypttab file and comment out the entries with a #
Run this command: update-initramfs -u -k all
```

Fstab:
---------------------
```
Open /etc/fstab and comment out every entry but the root entry. Replace the device location with the UUID of the device (by running blkid) or by its permanent path (/dev/vda1)
```

To restore grub, run these commands:
---------------------
```
apt update && apt install grub2 -y
install-grub /dev/vda (or whatever your device is)
update-grub
```

Now unmount all the things:
---------------------
```
exit
for i in {boot/efi,boot,dev/pts,dev,proc,sys}; do umount /$i /mnt/system/$i; done
umount /mnt/system
Reboot the VM, everything should "just work"
```

Full Rebuild
====================
Here’s how you rebuild the drive from scratch. It’s a lot more involved.
In a live ubuntu USB, do these steps:

```
mkdir /mnt/usb
mkdir /mnt/squash
mount /dev/sdx1 /mnt/usb # mount your usb
mount -o loop -t squashfs /mnt/usb/path/to/backup.sfs
cd /mnt/squash
ls
Hopefully there’s a file in the backup root called “LVM_Structure.bak” that will be a roadmap for your partitioning/LVM rebuild
```

---------------------
Partition out your drive. Here’s an example, done in commands
For BIOS Legacy: 
```
fdisk /dev/sdx
n
p
enter to accept defaults
enter to accept defaults
+800M
n
p
enter to accept defaults
enter to accept defaults
enter to accept defaults
a
1
w
Q
```
For UEFI:
```
gdisk /dev/sdx
n
enter to accept defaults
enter to accept defaults
+600M
Type: ef00
n
enter to accept defaults
enter to accept defaults
+800M
n
enter to accept defaults
enter to accept defaults
enter to accept defaults
w
```

Make the filesystem for EFI and/or boot

```
mkfs.fat -F 32 /dev/sdx1
mkfs.ext4 /dev/sdx2
Encrypt root drive
cryptsetup luksFormat /dev/sdx3
cryptsetup luksOpen /dev/sdx3 root
```

You should now have this partition structure

```
Boot/efi -  600 MB bootable win95 FAT32
Boot on UEFI system - 800 MB ext4
Root - ext4, luks encrypted
```

Set up the LVM structure from before. If it wasn’t LVM, skip this step.

```
pvcreate /dev/mapper/(name of crypt ie root)
vgcreate new-vg /dev/mapper/(name of crypt ie root)
lvcreate -L 4G -n swap_01 new-vg
lvcreate -l100%FREE -n new-lv new-vg
mkswap /dev/mapper/new-vg-swap_01
mkfs.ext4 /dev/mapper new-vg-new-lv
```
Restore the backup
```
mkdir /mnt/usb
mkdir /mnt/squash
mount /dev/mapper/new-vg--new-lv /mnt/system
mkdir /mnt/system/boot
mount /dev/sdx2 /mnt/system/boot (for legacy, do /dev/sdx1)
mkdir /mnt/system/boot/efi
mount /dev/sdx1 /mnt/system/boot/efi (for efi only)
mount -o loop -t squashfs /mnt/usb/path/to/backup.sfs
rsync -aAXHv --exclude="lost+found" /mnt/squash/* /mnt/system/ (this will take awhile)
umount /mnt/squash
umount /mnt/usb
```
This should make a copy of your archive to the drive. We need to chroot into it to update the fstab, crypttab and grub

Chroot:
---------------------
```
for i in {dev,dev/pts,proc,sys}; do mount --bind /$i /mnt/system/$i; done
chroot /mnt/system
```

Crypttab:
---------------------
+ Make sure no other luks drives are unlocked
+ Get the blkid of the partition where luks exists (/dev/sdx3)
+ Edit the /etc/crypttab file with the name and blkid of your drive
+ Run this command: update-initramfs -u -k all
+ If you get any crypttab errors, you need to check your UUID again, because something is wrong. The name and the UUID both have to match the drive exactly
You will need to specify where root, efi, boot and swap are on the drive. Edit the /etc/fstab file to reflect it using their UUIDs

Run blkid to get the UUID of the new drives partitions and erase out the old + entries.
```
blkid /dev/sdx2
blkid /dev/sdx1
blkid /dev/mapper/new-vg--new-lv
blkid /dev/mapper/new-vg--swap_01
vim /etc/fstab and throw them in the proper places. If boot is no longer a 
```
separate partition, then comment it out.
To restore grub, run these commands:

+ UEFI - 
```
Grub-install --boot-directory=/boot --efi-directory=/boot/efi --target=x86_64-efi /dev/sdx
update-grub
```
Note, if you get modprobe errors, you’ll have to purge grub and install grub-efi again. To do this, you’ll need an internet connection piped into your chroot. Connect the host machine to the internet, delete the /mnt/system/etc/resolv.conf file. Copy the /etc/resolv.conf file to /mnt/system/etc/. You should now have internet. Run sudo apt purge grub\*, sudo apt update, sudo apt install grub-efi 

+ Legacy BIOS - 
```
grub-install /dev/sdx
update-grub
```

Now unmount all the things:
```
exit
for i in {boot/efi,boot,dev/pts,dev,proc,sys}; do umount /$i /mnt/system/$i; done
umount /mnt/system
Reboot, everything should work normally.
```
