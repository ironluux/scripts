#!/bin/bash
#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# -----------------------------------------------------------------------------------------------------------

# This script should back up all files using rsync and might be a good alternative to doing a full disk image. This may be ran on a cron job, as well. Read the README.md for more info.
# Created on 12/3/2021

# Setting Variables - backup location and other folders I don't want backed up.
BACKUPFOLDER="/local_backup/"
ARCHIVE="/local_backup_archive/" 
BACKUP=/backups
VMLOCATION=/srv/* # If you have a location full of VMs like some people
NOW="$(date +%Y-%m-%d-%H%M%S)"

# Backin' up, beep beep beep beep
echo "Starting backup..."
rsync -aAXHv / --exclude=/swap.img --exclude=$VMLOCATION --exclude=$BACKUP/* --exclude=/home/*/.local/share/Trash/* --exclude=/dev/* --exclude=/proc/* --exclude=/sys/* --exclude=/tmp/* --exclude=/run/* --exclude=/mnt/* --exclude=/media/* --exclude=/lost+found $BACKUP$BACKUPFOLDER

# Compresses and moves the files to an archive
echo "Doing the squishy squish. Compressing as squashfs .sfs file using lz4 encryption yall AND I AM EXCITED ABOUT IT..."

# Compressing to squashfs
mksquashfs $BACKUP$BACKUPFOLDER* $BACKUP$ARCHIVE$NOW.sfs -comp lz4

# If you want full backup and not incremental, uncomment this command.
# Pros and cons of full backup - You could argue each archive is a full backup, but if a file becomes corrupted and infected the backups, you would never know. Might be good to start out fresh
# each time, if you're worried about that. It will take significantly longer though. Several hours as opposed to an hour or two. I guess if you're worried about it, you could move your first
# archive to a different folder and keep it as a good clean backup.
rm -rf $BACKUP$BACKUPFOLDER*

# Deletes archives that are 30 days or older
echo "Looking for those backup archives that are 30 days older and deleting those suckers..."
find $BACKUP$ARCHIVE -type f -name '*.sfs' -mtime +30 -execdir rm '{}' \;

echo "All done!"

exit 0