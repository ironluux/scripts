# Compiling OpenWrt For Linksys MX4300
### Downloading the Firmware
Currently, [this repo](https://github.com/arix00/openwrt-mx4300) has been the best source for firmware. This firmware uses the NSS proprietary features and has been somewhat stable. It's a nightly build though.

Download the zip and extract it

### Compiling the Firmware
1. cd into the zip folder
2. Run `./scripts/feeds update -a` to update the packages
3. Run `./scripts/feeds install -a` to install the updated packages
4. Run `make menuconfig` to open the make menu. Select these options:
    * Target System:       **Qualcomm Atheros 802.11ax WiSoC-s**
    * Subtarget:           **Qualcomm Atheros IQ807x**
    * Target Profile:      **Linksys MX4300**
    * Save as the `.config` file and exit
5. Run `make` to start compiling. This will take a long time, run overnight
6. Grab the bin files in `./bin/targets/qualcommax/ipq807x/`, these are your install files
7. You're done! Flash them on the router.

### Post Flash
After flashing, you'll need to install luci to connect to your router.
1. ssh into your router `ssh root@192.168.1.1`
2. Run `opkg update` to update the repos
3. Run `opkg install luci` to install luci
4. In a browser, go to 192.168.1.1 and login with a blank password
