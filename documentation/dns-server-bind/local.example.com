;
; BIND data file for the local loopback interface
;
$ORIGIN local.example.com.
$TTL    604800
@       IN      SOA     ns1.local.example.com. root.local.example.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;

; NS records for name servers
@    IN      NS      ns1.local.example.com.

; A records for name servers
ns1.local.example.com.		IN      A	192.168.10.20

; A records for domain names
rhel.local.example.com.		IN      A	192.168.10.30
coolsite.local.example.com.	IN      A	192.168.10.40
