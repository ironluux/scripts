# Bind Server for RHEL 9
Making a DNS server based on bind is fairly straightforward. The setup can be a little heavy but maintaining the DNS records is easy.

> **Note:** This tutorial doesn't cover having two or more DNS servers. That's pretty important for redundancy. This is mostly a tutorial on how to set this up at home or in a lab environment.

# Installation and Firewall Rules
1. To install bind, run this command - 
- `sudo dnf install bind bind-utils`
2. After that, open it up on your firewall - 
- `sudo firewall-cmd --add-service=dns --permanent && sudo firewall-cmd --reload`
3. Start/enable the named service (this is the main service for bind) - 
- `sudo systemctl enable --now named`
4. Edit the `/etc/named.conf` file 
- Change listen-on port 53 to also include your server's IP. Example - `listen-on port 53 { 127.0.0.1; 192.168.10.20; };`
- Change allow-query to point to the subnet your DNS server is on. Example - `allow-query { localhost; 192.168.10.0/24; };`
- Have recursion set to yes.
- Add the forwards below in the options { section. This is for forwarding requests to another DNS server if your local DNS server doesn't know the answer.

```
forwarders {
        1.1.1.1;
        9.9.9.9;
};
``` 

# Adding Your Zones
### Zones Explained
All zones are hosted in the `/etc/named.conf` file. It may be a good idea to back this file up before messing with it.

We will add two zones:
1. Our main zone
2. A zone for our subnet (this is for pointer records.)

The reason we need a zone from our subnet is so we can reverse lookup an address. If you only have the IP, you will be able to query that IP to find out what its DNS name is. Kinda cool, right?

> **Note:** You have to have an entry in your regular zone file and your ptr file for each server for reverse DNS queries to work. It's sort of a pain.

### How To Add Zones To /etc/named.conf
At the bottom of `/etc/named.conf`, paste the template below. Note that zone 2 is backwards from what you'd realistically think. For a /24, you'll need to put the IP range in backwards and leave the last octet off. For instance, 192.168.10.X/24 becomes 10.168.192. I know, confusing.

```
zone "local.example.com" IN {
    type master;
    file "/etc/bind/zones/local.example.com";
    allow-update {none;};
};

zone "10.168.192" IN {
    type master;
    file "/etc/bind/zones/ptr.10.168.192";
    allow-update {none;};
};
```

### Adding Your Zone Files
You may have noticed that we pointed those zones to `/etc/bind/zones/(nameofzone)`, we now need to create that file. This is where you will be adding your DNS records. 

For simplicity, I've uploaded examples of these files so you can get an idea of what these should look like. The main NS record should be the server you've installed bind on, the other stuff will be your servers. I've named mine ns1.local.example.com.

After each add, you have to restart your service - 
+ Check the syntax - `sudo named-checkconf` and `sudo named-checkzone local.example.com /etc/bind/zones/local.example.com` (make sure to do this for the ptr zone too.)
+ `sudo systemctl restart named`

# Pointing Your Servers to the Right DNS Server
After this, point your servers to your DNS server using its IP address (192.168.10.20). Also, be sure to update your search domains to the DNS server's DNS name (like ns1.local.example.com.) What this will do is allow you to use just the hostname of the server instead of the FQDN. So, if you have a server at rhel.local.example.com, you can just use rhel as the name. Pretty cool, right?
