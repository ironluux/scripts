# LVM Basics
LVM is a logical layer that sits on top of partitions. It allows you to easily expand or manage drives in linux.

There are 3 aspects to LVM:
1. The physical volume (pv) - this is the physical space with the data that gets allocated to the volume group and logical volume.
2. The volume group (vg) - the group using the physical volume. This is grouped in logical volumes.
3. The logical volume (lv) - the volume inside a volume group. This is what gets mounted and is used as a partition.

# Adding a New Disk to an Existing LVM
1. Add a new disk to the VM or server
2. Run these commands:
- `pvcreate /dev/sdX` - This creates the new disk to be used as lvm
- `vgextend{name of volume group}/dev/sdX` - extends the targetted volume group
- `vgdisplay` - shows the allocated space
- `lvextend --resize-fs -l +100%FREE` or `lvextend --resize-fs -L +10G /dev/mapper/{volumegroup}-{lvm you're targetting}` - This extends the logical volume with the available data from the pv/vg
> **Note:** If you didn't run `--resize-fs`, run this command below:
`resize2fs /dev/mapper/{volumegroup}-{lvm you're targetting}`

# Adding More Space to Existing Drive (VMs)
This is for VMs. This would be used when you add more space to the drive on the hypervisor and you need to allocate it. This only works if the main partition is the last partition on your hard drive. If you put swap as the last partition, welp, sorry.

1. On the hypervisor, add more space to the drive
2. Rescan the drive - logged in as root, run `echo 1> /sys/class/block/sda/device/rescan`
3. On the VM, open parted on the hard drive you just expanded
4. Type `print free` to make sure there is free data
5. Type `resizepart` with the partition number to group and by how much. It'll be where the main lvm is, probably partition 3. So, `resizepart 3 100%`
6. Type `sudo pvresize /dev/sdX3` - This is to grow the physical volume with the data we grew in the partition.
7. Type `pvdisplay` to verify there's extra data to be used
8. Type `sudo lvextend -r -l +100%FREE /dev/mapper/{volumegroup}-{lvm you are targetting}`
> **Note:** `-r` is the same thing as `resize-fs`
9. Verify that it's been resized by running `df -h`
