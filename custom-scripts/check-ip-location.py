#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# ----------------------------------------------------------------------------------------------------------
# Created on 4/11/2023
# Quick and dirty script to pull IPs from an .xlsx excel file and tell me which IPs are in the US.

# Uses pandas

# Imports
import requests
import json
import pandas as pd

# Parser that parses the .xlsx file and throws it into an array
df = pd.read_excel('check-ip-location-data.xlsx')
ipAddress = df['sourceIPAddress'].tolist()

# For loop that goes through each IP in an array
for i in ipAddress:
    response = requests.get(f'https://ipapi.co/{i}/json/').json()

    # If statement that grabs only the US based IPs
    if response.get("country_name") == 'United States':
        location_data = {
            "ip": i,
            "city": response.get("city"),
            "region": response.get("region"),
            "country": response.get("country_name")
        }
        # Prints the results to output
        print(location_data)
