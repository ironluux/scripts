#!/bin/bash
#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# -----------------------------------------------------------------------------------------------------------
# Intended to use with cron to auto-log if there are any issues with the network.
# Let me tell you a little story - back in the day, my house had faulty coax wiring. 
# Every day, the internet would become spotty, usually in the afternoon when more traffic was being pushed.
# Errors were piling up and I couldn't determine why. This log file was to find a correlation between 
# time of day and internet issues. I intended to send it to my ISP for help, but then found out that
# the cable in the attic was frayed and bent in several places. It took awhile, but I replaced the whole
# cable and now my internet has been running like a champ for years.

# Anyway, hope this script helps you in a similar manner. It's designed to ping a host, triggered by cron, and
# to write the results in a file if a certain failure threshold is met. Mine was set to 20%. There is no
# output associated with it so it can run lean and it will only log when the threshold is met. Enjoy!

# Variables
HOST="google.com" # What you'd like to ping
COUNT=50 # How many packets it will send, only set it 5 or higher
THRESHOLD=20 # The threshold that's acceptable, in percentage. 20% of successful packets and below will trigger a log entry.
DATE=$(date)
PACKETPERCENTAGE=$(($COUNT*$THRESHOLD/100))
LOGLOCATION=./ping-cron-log.txt # Log file location where it will log any time the threshold of failure is breached.

# Packet Pinging
SUCCESSFULPACKETS=$(ping -c $COUNT $HOST | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }') # Successful packets from ping
DROPPEDPACKETS=$(($COUNT-$SUCCESSFULPACKETS)) # Dropped packet calculation

if [ $DROPPEDPACKETS -gt $PACKETPERCENTAGE ]; then
	# What is sent to the log file
	echo "$DATE - Packet loss was at $THRESHOLD% or below with $DROPPEDPACKETS packets lost out of $COUNT. That means there were $SUCCESSFULPACKETS successful packets while pinging $HOST." >> $LOGLOCATION
fi

exit 0
