# Specific readme instructions for scripts
##### check-ip-location.py Installation
You will need to install pandas
`pip install pandas`

Make sure you have your .xlsx IP file ready to go named 'check-ip-location-data.xlsx' with the IPs being in column A.

##### check-ssl-expiration.sh
Just run it, you may want to edit the site list inside the script
`bash check-ssl-expiration.sh`

##### ping-cron.sh
Set up a cron job that will run the script. Inside the folder that you have the script in, if it gets triggered, you will see a 'ping-cron-log.txt' file generated.

##### is-the-port-open.sh
Very simple script to check if a port is open from a server to another server. Used to diagnose firewall issues.
