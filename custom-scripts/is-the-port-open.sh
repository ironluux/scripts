#!/bin/bash

#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# ----------------------------------------------------------------------------------------------------------
# Check to see if a port is open to a server. Very simple.

# Colors
CLEAR="\e[0m"
REDCOLOR="\e[31m"
GREENCOLOR="\e[32m"

# Grabbing neccessary variables
read -p "What server or IP are you trying to reach?
> " SRVNAME

read -r -p "What TCP ports are you checking? Seperate by spaces (example - 20 21 22 80)
Ports> " -a TCPPORTS

echo "--- Checking if $SRVNAME can be reached from this server ($HOSTNAME) on TCP Port(s): ${TCPPORTS[@]} ---"

# Where the magic happens - grabbing ports and using nc to test
for i in ${TCPPORTS[@]}; do
	NC=$(nc -z --wait 5 $SRVNAME $i && echo $?)

	# Logic check, checks to see if the variable isn't empty and if it equals 0. This is NC's success exit code.
	if [[ ! -z $NC && $NC -eq 0 ]]; then
		echo -e "$(echo $GREENCOLOR)*** TCP Port $i is open on $SRVNAME from this server ($HOSTNAME) ***$CLEAR"
	else
		echo -e "$(echo $REDCOLOR)*** TCP Port $i is closed on $SRVNAME from this server ($HOSTNAME) ***$CLEAR"
	fi
done

echo -e "Done"

exit 0
