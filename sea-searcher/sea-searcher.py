#Copyright (c) 2024 ironluux

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# ----------------------------------------------------------------------------------------------------------
# This script is designed to search for random things on bing using a text file as input. Please read the README.md to install properly.

# Setting up imports
import os
from time import sleep
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.edge.options import Options as EdgeOptions

# Turn off annoying rendering error bug.
options = EdgeOptions()
options.add_experimental_option('excludeSwitches', ['enable-logging'])
# Specifying drivers for edge so we can use edge to search with options
driver = webdriver.Edge(options=options)
# How long the browser will wait until it gives up
driver.implicitly_wait(10)
driver.maximize_window()

# Runs through the Moby Dick file from the book Moby Dick to select random words.
# URL to Moby Dick, which is loaded right now. https://www.gutenberg.org/cache/epub/2701/pg2701.txt
with open("./pg2701.txt",encoding="utf8") as file: 
    allText = file.read() 
    wordList = list(map(str, allText.split()))

# Login Info
username = "email@example.com"
password = "Your-Password"


# Log me in if I'm not logged in
try:
  driver.get("https://www.bing.com")
  sleep(2)
  driver.find_element(By.XPATH, "//a[@id='id_l']").click()
  sleep(5)
  driver.find_element(By.XPATH, "//input[@type='submit']")
  driver.find_element(By.NAME, "loginfmt").send_keys(username)
  driver.find_element(By.XPATH, "//input[@type='submit']").click()
  sleep(4)
  driver.find_element(By.NAME, "passwd").send_keys(password)
  driver.find_element(By.XPATH, "//input[@type='submit']").click()
  sleep(3)
  driver.find_element(By.XPATH, "//input[@type='submit']").click()
  sleep(5)
  print("Logged you in!")
except:
  print("No login button detected, either the user is logged in or there's an error, moving on.")

# Runs through 5 searches every 16 minutes ten times. Because, seriously, bing???
for iterations in range(20):
  for searches in range(5):
    try:
      driver.get("https://www.bing.com")
      randomSearch = random.choice(wordList) + " " + random.choice(wordList)
      driver.find_element(By.ID, "sb_form_q").send_keys(randomSearch)
      driver.find_element(By.ID, "sb_form_q").send_keys(Keys.RETURN)
      randomNumber=random.randint(14,32)
      sleep(randomNumber)
      driver.find_element(By.ID, "sb_form_q").clear()
      print("Successfully searched 1 time!")
    except:
      print("Error: Couldn't search, something went wrong, skipping...")
  sleep(1200) # This is 20 minutes
  driver.get("https://www.bing.com") #reset
  print("Iterated through searches 1 time!")
