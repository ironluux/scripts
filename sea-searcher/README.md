# Sea Searcher
This script searches random words on Bing using Moby Dick as the source material... for no reason at all.

### Installation
##### Dependencies
- You must install selenium python to use this script: `pip install selenium`
- Download the [latest copy of Moby Dick here](https://www.gutenberg.org/cache/epub/2701/pg2701.txt) and add it to the same folder you're running the script from.
- You have to have Edge installed and named msedgedriver.exe inside the same folder you're running the script from. Grab the latest edge drivers here: [Edge Drivers For Selenium](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/?form=MA13LH)

##### How To Run
1. Open the script and add your login credentials to the script
2. Make sure the updated webdrivers and the copy of Moby Dick are in the folder you're going to run the script from. 
3. Run the script by typing `python3 sea-searcher.py`
4. Leave it up, let it run. It will take 3.5 to 4 hours to run. Each interation takes roughly 20 minutes.
5. Enjoy the high seas!
