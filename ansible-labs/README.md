# ansible-labs
This repo holds my ansible labs and playbooks.

##### Installation Guide
- Generate an ssh key called ansible `ssh-keygen -t ed25519 -C "Ansible Key"`
- Run the `install_mgmt_user.yml` file using the user you set up on your servers. Name the user `ansibleuser`
- Uncomment the ansibleuser lines and keyfile lines in `ansible.cfg`
- Test it all out, make sure it works with the user and keyfile
